'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();
var globbing = require('gulp-css-globbing');
var autoprefixer = require('gulp-autoprefixer');
var cssImageDimensions = require('gulp-css-image-dimensions');
var greplace = require('gulp-replace');
// var path = require('path');
// var kss  = require('kss');

gulp.task('server-start', ['sass'], function(){
  gulp.watch(['sass/**.scss', 'sass/**/**.scss'], ['sass']);
  gulp.watch('**.html').on('change', browserSync.reload);
});

gulp.task('sass', function () {
    gulp.src(['sass/*/**.scss'])
      .pipe(plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
      }}))
      .pipe(globbing({
        extensions: ['.scss']
      }))
      .pipe(greplace(/^\s*(@import\s+)?url\((["'][^"'\)]+['"])(?:\))?(;)?\s*$/gm, '$1$2$3'))
      .pipe(sass({
        includePaths: [ './scss' ],
        errLogToConsole: true,
        outputStyle: 'compact'
      }))
      .pipe(autoprefixer({
        browsers: ['last 2 versions', 'ie >= 6', 'chrome >= 4', 'ff >= 3'],
        cascade: false
      }))
      .pipe(cssImageDimensions('../../images'))
      .pipe(gulp.dest('./css'))
      .pipe(browserSync.stream())
});

gulp.task('default', ['server-start']);

//=======================================================
// Generate style guide
//=======================================================
/*
gulp.task('styleguide', function() {
  return kss({
    source: [
      'components',
    ],
    destination: './dist/style-guide',
    builder: 'style-guide/builder',
    namespace: 'seed:' + __dirname + '/components/',
    'extend-drupal8': true,
    // The css and js paths are URLs, like '/misc/jquery.js'.
    // The following paths are relative to the generated style guide.
    css: [
      path.relative(
        __dirname + '/style-guide/',
        __dirname + '/css/global.css'
      )
    ],
    js: [
    ],
    homepage: 'style-guide.md',
    title: 'Style Guide'
  });
});
*/

