# Seed Theme - Sub theme
The templates folder is where you should put all your template files.
Our recommendation here is to us subfolders like this:

* layout
	* page.html.twig
	* html.html.twig
* views
	* views-views-unformatted.html-twig
* ...

Again, it's just our way, feel free to use or not to use subfolders!


